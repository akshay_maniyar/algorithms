# Running the program
# python Adaboost.py t <training file> <testing file>

from __future__ import division
import argparse
import sys
import math
import random

__author__ = 'akshay'

def log(x):
    if(x == 0):
        return 0
    else:
        return math.log(x,2)

def getSplittingAttribute(trainingLines,weights):

    edi_wt = 0
    pos_wt = 0
    answers = [0]
    # find E(S)
    for i in range(len(trainingLines)):
        if trainingLines[i][0] == "e":
            edi_wt += weights[i]
        if trainingLines[i][0] == "p":
            pos_wt += weights[i]

    total_wt = edi_wt + pos_wt
    totalEntropy = -(edi_wt/total_wt)*log(edi_wt/total_wt) -(pos_wt/total_wt)*log(pos_wt/total_wt)

    ediDict = [dict() for x in range(len(trainingLines[0]))]
    posDict = [dict() for x in range(len(trainingLines[0]))]

    # find max information gain from all columns
    for i in range(len(trainingLines)):
        for j in range(1,len(trainingLines[i])):
            key = trainingLines[i][j]
            if trainingLines[i][0] == "e":
                if key not in ediDict[j]:
                    ediDict[j][key] = weights[i]
                    posDict[j][key] = 0
                else:
                    ediDict[j][key] += weights[i]
            if trainingLines[i][0] == "p":
                if key not in posDict[j]:
                    posDict[j][key] = weights[i]
                    ediDict[j][key] = 0
                else:
                    posDict[j][key] += weights[i]

    minEntropy = 999
    for i in range(1,len(ediDict)):
        entropy_col = 0
        for key in ediDict[i]:
            total = ediDict[i][key]+posDict[i][key]
            entropy_attr = total/sum(weights)*(-(ediDict[i][key]/total)*log(ediDict[i][key]/total) - (posDict[i][key]/total)*log(posDict[i][key]/total))
            entropy_col += entropy_attr

        answers.append(0.99-entropy_col)
        if entropy_col < minEntropy:
            minEntropy = entropy_col
            splittingColumn = i
    sortedIndex = (sorted(range(len(answers)), key=lambda k: answers[k],reverse=True))
    ans = [[i,answers[i]] for i in sortedIndex ]
    for i in ans:
        print i
    return splittingColumn,ediDict[splittingColumn],posDict[splittingColumn]

def main():
    parser = argparse.ArgumentParser(
        description="Adaboost with decision stump")
    parser.add_argument(
        "t", metavar="NUMBER_OF_ITERATION", type=int,
        help="Number of Iterations")
    parser.add_argument(
        "file1", metavar="TRAINING_FILE", type=argparse.FileType("r"),
        help="Training Data File")
    parser.add_argument(
        "file2", metavar="TESTING_FILE", type=argparse.FileType("r"),
        help="Testing Data File")
    parser.add_argument(
        "--output", dest="output", type=argparse.FileType("w"),
        default=sys.stdout, help="Output adaboost [default: stdout]")
    args = parser.parse_args()

    # store the training file
    trainingLines = [line.split() for line in args.file1.read().splitlines()]

    # store the testing file
    testingLines = [line.split() for line in args.file2.read().splitlines()]

    # initialize the weights
    weights = [1/len(trainingLines)]*len(trainingLines)

    alphas = []
    splittingColumns = []
    decisionTrees = [dict() for x in range(args.t)]

    for iteration in range(args.t):
        splittingColumn,ediDict,posDict = getSplittingAttribute(trainingLines,weights)
        incorrectlyClassified = []
        incorrectWt = 0
        decisionTrees[iteration] = dict(ediDict)
        for i in range(len(trainingLines)):
            # using weights to classify samples
            if ediDict[trainingLines[i][splittingColumn]] > posDict[trainingLines[i][splittingColumn]]:
                classified = "e"
                decisionTrees[iteration][trainingLines[i][splittingColumn]] = "e"
            else:
                classified = "p"
                decisionTrees[iteration][trainingLines[i][splittingColumn]] = "p"

            # if correctly classified
            if classified != trainingLines[i][0]:
                incorrectlyClassified.append(i)
                incorrectWt += weights[i]

        epsilon = incorrectWt
        if epsilon != 0:
            alpha = 0.5*(math.log(((1-epsilon)/epsilon),math.e))
        else:
            alpha = 0

        #print epsilon
        alphas.append(alpha)
        splittingColumns.append(splittingColumn)

        # print weights
        # print "iteration:%s alpha:%.4f splittingAttribute:%s missclassified:%s decisionTree:%s" % (iteration,alpha,splittingColumn,len(incorrectlyClassified),decisionTrees[iteration])
        # print

        # update weights
        for i in range(len(weights)):
            if i in incorrectlyClassified:
                weights[i] *= math.pow(math.e,alpha)
            else:
                weights[i] *= math.pow(math.e,-alpha)

        # normalize weights
        sumWeights = sum(weights)
        for i in range(len(weights)):
            weights[i] = weights[i]/sumWeights

    incorrectHclassified = 0
    correctHclassified = 0
    for i in range(len(testingLines)):
        Hclassifier = 0
        for j in range(len(alphas)):
            if testingLines[i][splittingColumns[j]] in decisionTrees[j]:
                if decisionTrees[j][testingLines[i][splittingColumns[j]]] == "e":
                    mul = +1
                else:
                    mul = -1
                Hclassifier += alphas[j]*mul
            else:
                mul = random.randint(0,1)
                if mul == 0:
                    mul = -1
                Hclassifier += alphas[j]*mul

        if Hclassifier > 0:
            classified = "e"
        else:
            classified = "p"
        if classified != testingLines[i][0]:
            # print classified,testingLines[i][0]
            incorrectHclassified += 1
        else:
            correctHclassified += 1

    #print (correctHclassified)/(len(testingLines))
    for alpha in alphas:
        #print alpha
        pass


if __name__ == "__main__":
    main()
