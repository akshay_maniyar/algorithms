
class Graph:

    def __init__(self,v):
        self.v = v
        self.adj = [[] for i in range(v)]

    def addEdge(self,v1,v2):
        self.adj[v1].append(v2)

    def __str__(self):
        return str(self.adj)

    def bfs(self,start):
        queue = []
        visited = [False for i in range(self.v)]
        bfsSequence = []
        queue.append(start)

        while(len(queue) != 0):
            vertex = queue.pop()
            visited[vertex] = True
            bfsSequence.append(vertex)
            for v in self.adj[vertex]:
                if visited[v] == False:
                    queue.append(v)

        return bfsSequence
                    
    
def main():
    g = Graph(4)
    g.addEdge(0,2)
    g.addEdge(0,1)
    g.addEdge(1,2)
    g.addEdge(2,3)
    g.addEdge(3,3)
    g.addEdge(2,0)


    print ("Graph",str(g))
    print ("Bfs Sequence",g.bfs(2))

if __name__ == "__main__":
    main()
