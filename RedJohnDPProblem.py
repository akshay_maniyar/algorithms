# https://www.hackerrank.com/challenges/red-john-is-back
# Enter your code here. Read input from STDIN. Print output to STDOUT
import math
# Checks if number is prime
def isPrime(n):
    if n==2:
        return True
    for i in range(2,int(math.sqrt(n))+1):
        if n % i == 0:
            return False
       
    return True

# Returns all primes till the given number
def findPrimesTill(n):
    primes = []
    for i in range(2,n+1):
        if isPrime(i):
            primes.append(i)
    return primes

# Reads input and calculates combinations till N=41
T = int(raw_input())
input = []
for i in range(T):
    input.append(int(raw_input()))

number_ways = [1 for i in range(42)]
for i in range(4,len(number_ways)):
    number_ways[i] = number_ways[i-1]+number_ways[i-4]

for i in input:
    print(len(findPrimesTill(number_ways[i])))