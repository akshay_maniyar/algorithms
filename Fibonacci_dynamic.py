def fib(n):
    fib_arr = [0 for i in range(n+1)]
    fib_arr[0] = 0
    fib_arr[1] = 1
    
    if n < 2:
        return fib_arr[n]
        
    else:
        for i in range(2,n+1):
        	fib_arr[i] = fib_arr[i-1]+fib_arr[i-2]
        return fib_arr[n]
# O(n)        
        
def recFib(n):
	if n==0 or n==1:
		return n
	return recFib(n-1)+recFib(n-2)
#O(2^n)	

print (fib(50))
print (recFib(50))