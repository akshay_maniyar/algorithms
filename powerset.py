def powerset(set):
    format_info = "{0:0"+str(len(set))+"b}"
    binaries = [format_info.format(i) for i in range(pow(2,len(set)))]
    powerset = []
    for each in binaries:
        subset = []
        for i in range(len(each)):
            if each[i] == '1':
                subset.append(set[i])
        powerset.append(subset)
        
    return powerset
                

print powerset(['a','b','c'])