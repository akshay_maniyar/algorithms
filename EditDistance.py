# Find the Edit distance of one string from the other
# Three operations for editing the string
# 1. insert cost 1
# 2. delete cost 1
# 3. match(substitute) = if not matched substituting cose 1
# Solution using dynamic programming
 
def editDistanceRec(string1,string2):
	if string1 == "":
		return len(string2)
	if string2 == "":
 		return len(string1)
 		
	addDist = editDistanceRec(string1,string2[0:len(string2)-1]) + 1
	delDist = editDistanceRec(string1[0:len(string1)-1],string2) + 1
 	
	if string2[len(string2)-1] == string1[len(string1)-1]:
		matDist = editDistanceRec(string1[0:len(string1)-1],string2[0:len(string2)-1])
	else:
		matDist = editDistanceRec(string1[0:len(string1)-1],string2[0:len(string2)-1]) + 1
 	
	return min(matDist,delDist,addDist)
 	
print(editDistanceRec("attack","attked"))

def editDistanceDyn(string1,string2):
	m = len(string1)+1
	n = len(string2)+1
	# create a m*n array
	subResults = [[0 for i in range(n)] for i in range(m)]

	# add the base case in the matrix
	for i in range(1,m):
		subResults[i][0] = subResults[i-1][0] + 1
	for j in range(1,n):
		subResults[0][j] = subResults[0][j-1] + 1
	
	# Fill in the rest of the matrix based on the previous values
	
	for i in range(1,m):
		for j in range(1,n):
			if string1[i-1] == string2[j-1]:
				subResults[i][j] = \
				min(subResults[i-1][j]+1,subResults[i][j-1]+1, \
				subResults[i-1][j-1])
			else:
				subResults[i][j] = \
				min(subResults[i-1][j]+1,subResults[i][j-1]+1, \
				subResults[i-1][j-1]+1)
	
	return subResults[m-1][n-1]
	
print(editDistanceDyn("attack","attked"))
