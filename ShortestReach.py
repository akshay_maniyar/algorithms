'''
Problem Definition

Given an undirected graph consisting of NN nodes (labelled 1 to N) where a specific given node SS represents the start position and an edge between any two nodes is of length 66 units in the graph.

It is required to calculate the shortest distance from start position (Node S) to all of the other nodes in the graph.

Note 1: If a node is unreachable , the distance is assumed as −1−1. 
Note 2: The length of each edge in the graph is 66 units.

Input Format

The first line contains TT, denoting the number of test cases. 
First line of each test case has two integers NN, denoting the number of nodes in the graph and MM, denoting the number of edges in the graph. 
The next MM lines each consist of two space separated integers x yx y, where xx and yy denote the two nodes between which the edge exists. 
The last line of a testcase has an integer SS, denoting the starting position.

Constraints 
1≤T≤101≤T≤10 
2≤N≤10002≤N≤1000 
1≤M≤N×(N−1)21≤M≤N×(N−1)2 
1≤x,y,S≤N1≤x,y,S≤N

Output Format

For each of TT test cases, print a single line consisting of N−1N−1 space-separated integers, denoting the shortest distances of the N-1 nodes from starting position SS. This will be done for all nodes same as in the order of input 1 to N.

For unreachable nodes, print −1−1.

Sample Input

2
4 2
1 2
1 3
1
3 1
2 3
2
Sample Output

6 6 -1
-1 6
'''
# Enter your code here. Read input from STDIN. Print output to STDOUT
import sys

class Node:
    def __init__(self,name):
        self.name = name
        self.adjacents = set()
        
    def addAdjacents(self,Node):
        self.adjacents.add(Node)
    
    def printAdj(self):
        print [i.name for i in self.adjacents]
        
class Graph:
    def __init__(self,number_of_nodes):
        self.number_of_nodes = number_of_nodes
        self.nodes = [Node(i) for i in range(1,number_of_nodes+1)]
        
    def insert(self,node1,node2):
        node1.addAdjacents(node2)
    
    def printNodes(self):
        print [i.name for i in self.nodes]
        
    def findMinFrom(self,startNode):
        queue = []
        queue.append(startNode)
        visited = set()
        # distance = []
        # parent = {}
        distance = [-1 for i in range(self.number_of_nodes)]
        distance[int(startNode.name)-1] = 0
        # Keep adding Nodes untill all visited in breadth first manner
        while len(queue) != 0:
            vertex = queue.pop(0)
            visited.add(vertex)
            for adjs in vertex.adjacents:
                if adjs not in visited:
                    queue.append(adjs)
                    distance[int(adjs.name)-1] = distance[int(vertex.name)-1]+6
                    visited.add(adjs)
                    # if adjs not in parent:
                    #    parent[adjs] = vertex
        
        '''
        # Keep track of parents to find the path
        
        for node in self.nodes:
            dist = 0
            tempP = node
            if node == startNode:
                pass

            elif node not in parent:
                distance.append(str(-1))
            else:
                while tempP != startNode:
                    dist = dist + 6
                    tempP = parent[tempP]
                distance.append(str(dist))         
        '''
        del distance[int(startNode.name)-1]
        return distance
    
def main():
    lines = sys.stdin.readlines()
    newGraph = True
    for i in range(1,len(lines)):
        input_ = [int(j) for j in lines[i].split()]
        if newGraph == True:
            g = Graph(input_[0])
            newGraph = False
        elif len(input_) == 2:
            if g.nodes[input_[1]-1] not in g.nodes[input_[0]-1].adjacents:
                g.insert(g.nodes[input_[0]-1],g.nodes[input_[1]-1])
                g.insert(g.nodes[input_[1]-1],g.nodes[input_[0]-1])
        elif len(input_) == 1:
            start = g.nodes[input_[0]-1]
            print ' '.join([str(k) for k in g.findMinFrom(start)])
            newGraph = True
        
main()
        